package tk.yoshirealms.yrapi.config;

import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Stream;

public class ConfigBuilder {

    private String name;
    private File file;
    private HashMap<String, HashMap<String, String>> values = new HashMap<>();

    public ConfigBuilder(String name, JavaPlugin plugin) {
        this.name = name;
        this.file = new File(plugin.getDataFolder(), name + ".yml");
        fromFile(file);
    }

    public ConfigBuilder(String name, File file) {
        this.name = name;
        this.file = file;
        fromFile(file);
    }

    public ConfigBuilder fromFile(File file) {
        try {
            if (file.exists()) {
                try (Stream<String> stream = Files.lines(file.toPath())) {
                    List<String> comment = new ArrayList<>();
                    stream.forEach(s -> {
                        if (s.startsWith("#")) {
                            comment.add(s.replaceFirst("#", ""));
                        }
                    });
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    public ConfigBuilder add(String key, String name, String desc, String def) {
        if (!values.containsKey(key)) {
            HashMap<String, String> val = new HashMap<>();
            val.put("name", name);
            val.put("desc", desc);
            val.put("def", def);
            values.put(key, val);
        }
        return this;
    }

    public ConfigBuilder add(String key, String name, String desc, String def, String[] options) {
        if (!values.containsKey(key)) {
            HashMap<String, String> val = new HashMap<>();
            val.put("name", name);
            val.put("desc", desc);
            val.put("def", def);
            val.put("options", Arrays.toString(options));
            values.put(key, val);
        }
        return this;
    }

    public ConfigBuilder remove(String key) {
        if (values.containsKey(key))
            values.remove(key);
        return this;
    }

    public Config build() {
        return new Config(name, file, values);
    }


}
