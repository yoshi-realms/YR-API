package tk.yoshirealms.yrapi.config;

import java.io.File;
import java.util.HashMap;

public class Config {

    private String name;
    private File file;
    private HashMap<String, HashMap<String, String>> values = new HashMap<>();

    public Config(String name, File file, HashMap<String, HashMap<String, String>> values) {
        this.name = name;
        this.file = file;
        this.values = values;
    }

    public void save() {
        //TODO save config
    }
}
