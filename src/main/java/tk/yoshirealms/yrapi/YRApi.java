package tk.yoshirealms.yrapi;

import org.bukkit.plugin.java.JavaPlugin;

import java.util.logging.Level;
import java.util.logging.Logger;

public final class YRApi extends JavaPlugin {

    Logger logger = Logger.getLogger("Minecraft");

    @Override
    public void onEnable() {
        logger.log(Level.INFO, "Enabling YR-Api");
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
