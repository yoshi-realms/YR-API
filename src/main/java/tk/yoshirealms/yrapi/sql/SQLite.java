package tk.yoshirealms.yrapi.sql;

import java.sql.*;

public class SQLite extends SQLDriver {

    private Connection conn = null;


    public SQLite(String url) {
        try {
            conn = DriverManager.getConnection(url);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void execute(String quarry) {
        if (conn != null) {
            try {
                Statement statement = conn.createStatement();
                statement.execute(quarry);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public ResultSet executeQuarry(String quarry) {
        try {
            Statement statement = conn.createStatement();
            return statement.executeQuery(quarry);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
