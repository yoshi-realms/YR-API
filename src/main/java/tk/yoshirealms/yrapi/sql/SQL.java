package tk.yoshirealms.yrapi.sql;

import java.sql.ResultSet;

import static tk.yoshirealms.yrapi.sql.SQL.Type.MYSQL;
import static tk.yoshirealms.yrapi.sql.SQL.Type.SQLITE;

public class SQL {

    private Type type;
    private String url;
    private SQLDriver driver;

    /**
     * Create an SQL connection to either a mySQL or SQLite database.
     * @param type The type of the database as {@link Type}
     * @param url The url of the database being used as a {@link String}
     */
    public SQL(Type type, String url) {
        this.type = type;
        this.url = url;
        if (type == SQLITE)
            driver = new SQLite(url);
        else if (type == MYSQL)
            driver = new mySQL(url);
    }

    /**
     * Execute a quarry on the database.
     * @param quarry The quarry to execute as a {@link String}
     */
    public void execute(String quarry) {
        driver.execute(quarry);
    }

    /**
     * Execute a quarry and get a result from it
     * @param quarry The quarry to execute as a {@link String}
     * @return The {@link ResultSet} that the database returns
     */
    public ResultSet executeQuarry(String quarry) {
        return driver.executeQuarry(quarry);
    }

    /**
     * Close the database connection cutting off any results that are still being used
     */
    public void close() {
        driver.close();
    }

    public enum Type {
        SQLITE,
        MYSQL
    }
}

abstract class SQLDriver {
    public abstract void execute(String quarry);
    public abstract ResultSet executeQuarry(String quarry);
    public abstract void close();
}


