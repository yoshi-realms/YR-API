package tk.yoshirealms.yrapi.sql;

import java.sql.*;

public class mySQL extends SQLDriver {

    private Connection conn = null;

    public mySQL(String url) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            conn = DriverManager.getConnection(url);
        } catch (ClassNotFoundException | SQLException e) {
            System.out.println("Could not find mysql driver or url is wrong");
            e.printStackTrace();
        }
    }

    @Override
    public void execute(String quarry) {
        if (conn != null) {
            try {
                Statement statement = conn.createStatement();
                statement.execute(quarry);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Connection is not open");
        }
    }

    @Override
    public ResultSet executeQuarry(String quarry) {
        if (conn != null) {
            try {
                Statement statement = conn.createStatement();
                return statement.executeQuery(quarry);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {
            System.out.println("Connection is not open");
        }
        return null;
    }

    @Override
    public void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
